import { EXTENSION_ID } from "../../extension/constants";
import OBR from "@owlbear-rodeo/sdk";
import createIconUrl from "../../font-awesome/createIconUrl";
import { DRAWING_TOOL_ID } from "../../obr/tools/constants";
import cut from "./cut";
import { Metadata } from "@owlbear-rodeo/sdk/lib/types/Metadata";

const KEEP_CLOSED_METADATA_ID = `${EXTENSION_ID}.tool-metadata.is-keep-closed-enabled`;

export default function createCutMode() {
  const CUT_MODE_ID = `${EXTENSION_ID}.tool-mode.cut`;
  OBR.tool.createMode({
    id: CUT_MODE_ID,
    icons: [
      {
        icon: createIconUrl("scissors-solid.svg"),
        label: "Cut",
        filter: {
          activeTools: [DRAWING_TOOL_ID],
          permissions: ["DRAWING_UPDATE"],
        },
      },
    ],
    cursors: [
      {
        cursor: "crosshair",
      },
    ],
    onToolClick: (context, event) => {
      const effectiveValue = getEffectiveValue(context.metadata);
      return cut(effectiveValue, event.pointerPosition, event.target);
    },
  });

  OBR.tool.createAction({
    id: `${EXTENSION_ID}.tool-mode.cut.foo`,
    icons: [
      {
        icon: createIconUrl("circle-check-regular.svg"),
        label: "Closed polygons stay closed when cut. Click to change.",
        filter: {
          activeTools: [DRAWING_TOOL_ID],
          // activeModes: [CUT_MODE_ID],
          metadata: [
            {
              key: KEEP_CLOSED_METADATA_ID,
              value: true,
              coordinator: "||",
            },
            {
              key: KEEP_CLOSED_METADATA_ID,
              value: undefined,
            },
          ],
        },
      },
      {
        icon: createIconUrl("circle-notch-solid.svg"),
        label: "Closed polygons are opened when cut. Click to change.",
        filter: {
          activeTools: [DRAWING_TOOL_ID],
          // activeModes: [CUT_MODE_ID],
          metadata: [
            {
              key: KEEP_CLOSED_METADATA_ID,
              value: false,
            },
          ],
        },
      },
    ],
    onClick: (context) => {
      const effectiveValue = getEffectiveValue(context.metadata);
      OBR.tool.setMetadata(context.activeTool, {
        [KEEP_CLOSED_METADATA_ID]: !effectiveValue,
      });
    },
  });
}

function getEffectiveValue(metadata: Metadata): boolean {
  const currentValue = metadata[KEEP_CLOSED_METADATA_ID];
  return currentValue === undefined ? true : (currentValue as boolean);
}
