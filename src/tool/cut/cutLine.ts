import OBR, { Line, Vector2 } from "@owlbear-rodeo/sdk";
import cloneLine from "../../obr/items/cloneLine";
import { calculateD, convertGlobalToItemPosition } from "./math";

export default function cutLine(line: Line, pointerPosition: Vector2) {
  const newLine = cloneLine(line);
  newLine.startPosition = calculateCut(newLine, pointerPosition);

  OBR.scene.items.addItems([newLine]);
  OBR.scene.items.updateItems([line], (items) => {
    for (let item of items) {
      item.endPosition = { ...newLine.startPosition };
    }
  });

  OBR.player.select([newLine.id]);
}

function calculateCut(line: Line, pointer: Vector2): Vector2 {
  const a = line.startPosition;
  const b = line.endPosition;
  const c = convertGlobalToItemPosition(line, pointer);
  return calculateD(a, b, c);
}
