import OBR, {
  isCurve,
  isLine,
  isShape,
  Item,
  Vector2,
} from "@owlbear-rodeo/sdk";
import cutLine from "./cutLine";
import cutCurve from "./cutCurve";
import cutShape from "./cutShape";

export default function cut(
  isKeepClosedEnabled: boolean,
  pointer: Vector2,
  item?: Item,
) {
  if (!item) {
    return true;
  }

  if (isLine(item)) {
    cutLine(item, pointer);
    return;
  }

  if (isCurve(item)) {
    cutCurve(isKeepClosedEnabled, item, pointer);
    return;
  }

  if (isShape(item)) {
    cutShape(isKeepClosedEnabled, item, pointer);
    return;
  }

  OBR.notification.show(`Type is not supported: ${item.type}`, "ERROR");
}
