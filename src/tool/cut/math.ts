import { Item, Vector2 } from "@owlbear-rodeo/sdk";

export function convertGlobalToItemPosition(
  item: Item,
  globalPosition: Vector2,
): Vector2 {
  const x = globalPosition.x - item.position.x;
  const y = globalPosition.y - item.position.y;
  const r = deg2rad(item.rotation);
  return {
    x: (x * Math.cos(r) + y * Math.sin(r)) / item.scale.x,
    y: (y * Math.cos(r) - x * Math.sin(r)) / item.scale.y,
  };
}

function deg2rad(degress: number) {
  return (degress * Math.PI) / 180;
}

export function convertItemToGlobalPosition(
  item: Item,
  itemPosition: Vector2,
): Vector2 {
  const x = itemPosition.x;
  const y = itemPosition.y;
  const r = deg2rad(item.rotation);
  return {
    x: (x * Math.cos(r) - y * Math.sin(r)) * item.scale.x + item.position.x,
    y: (y * Math.cos(r) + x * Math.sin(r)) * item.scale.y + item.position.y,
  };
}

export function calculateD(a: Vector2, b: Vector2, c: Vector2): Vector2 {
  const t =
    ((c.x - a.x) * (b.x - a.x) + (c.y - a.y) * (b.y - a.y)) /
    (Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2));
  return {
    x: a.x + t * (b.x - a.x),
    y: a.y + t * (b.y - a.y),
  };
}

export function calculateDistance(a: Vector2, b: Vector2) {
  return Math.sqrt(Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2));
}
