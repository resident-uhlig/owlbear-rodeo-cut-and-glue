import OBR, { buildCurve, Curve, Shape, Vector2 } from "@owlbear-rodeo/sdk";
import cloneItem from "../../obr/items/cloneItem";
import cutCurve from "./cutCurve";

export default function cutShape(
  isKeepClosedEnabled: boolean,
  shape: Shape,
  pointer: Vector2,
) {
  switch (shape.shapeType) {
    case "CIRCLE":
      cutCircle(isKeepClosedEnabled, shape, pointer);
      return;

    case "RECTANGLE":
      cutRectangle(isKeepClosedEnabled, shape, pointer);
      return;

    case "TRIANGLE":
      cutTriangle(isKeepClosedEnabled, shape, pointer);
      return;

    case "HEXAGON":
      cutHexagon(isKeepClosedEnabled, shape, pointer);
      return;
  }

  OBR.notification.show(
    `Shape is not supported: ${shape.shapeType}`,
    "WARNING",
  );
}

function cutCircle(
  isKeepClosedEnabled: boolean,
  shape: Shape,
  pointer: Vector2,
) {
  const curve = convert(shape);

  curve.points = [];
  const radius = Math.min(shape.width, shape.height) / 2;
  const numberOfPoints = 20;
  const angleIncrement = (2 * Math.PI) / numberOfPoints;
  for (let i = 0; i < numberOfPoints; i++) {
    const angle = i * angleIncrement;
    const x = radius * Math.cos(angle);
    const y = radius * Math.sin(angle);
    curve.points.push({ x, y });
  }

  replaceAndCut(isKeepClosedEnabled, shape, curve, pointer);
}

function convert(shape: Shape) {
  return cloneItem(buildCurve(), shape)
    .style({ ...shape.style, tension: 0, closed: true })
    .build();
}

function replaceAndCut(
  isKeepClosedEnabled: boolean,
  shape: Shape,
  curve: Curve,
  pointer: Vector2,
) {
  OBR.scene.items.deleteItems([shape.id]);
  OBR.scene.items.addItems([curve]).then(() => {
    cutCurve(isKeepClosedEnabled, curve, pointer);
  });
}

function cutRectangle(
  isKeepClosedEnabled: boolean,
  shape: Shape,
  pointer: Vector2,
) {
  const curve = convert(shape);

  const x0 = 0;
  const y0 = 0;
  const x1 = x0 + shape.width;
  const y1 = y0 + shape.height;
  curve.points = [
    { x: x0, y: y0 },
    { x: x0, y: y1 },
    { x: x1, y: y1 },
    { x: x1, y: y0 },
  ];

  replaceAndCut(isKeepClosedEnabled, shape, curve, pointer);
}

function cutTriangle(
  isKeepClosedEnabled: boolean,
  shape: Shape,
  pointer: Vector2,
) {
  const curve = convert(shape);

  const x = 0;
  const y = 0;
  const width = shape.width / 2;
  const height = shape.height;
  curve.points = [
    { x, y },
    { x: x - width, y: y + height },
    { x: x + width, y: y + height },
  ];

  replaceAndCut(isKeepClosedEnabled, shape, curve, pointer);
}

function cutHexagon(
  isKeepClosedEnabled: boolean,
  shape: Shape,
  pointer: Vector2,
) {
  const curve = convert(shape);

  curve.points = [];
  const radius = shape.width / 2;
  const angles = [
    Math.PI / 2,
    Math.PI / 6,
    (11 * Math.PI) / 6,
    (3 * Math.PI) / 2,
    (7 * Math.PI) / 6,
    (5 * Math.PI) / 6,
  ];
  for (let i = 0; i < 6; i++) {
    const angle = angles[i];
    const x = radius * Math.cos(angle);
    const y = radius * Math.sin(angle);
    curve.points.push({ x, y });
  }

  replaceAndCut(isKeepClosedEnabled, shape, curve, pointer);
}
