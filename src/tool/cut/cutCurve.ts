import OBR, { Curve, Vector2 } from "@owlbear-rodeo/sdk";
import cloneCurve from "../../obr/items/cloneCurve";
import {
  calculateD,
  calculateDistance,
  convertGlobalToItemPosition,
} from "./math";
import isClosed from "../../obr/items/isClosed";

export default function cutCurve(
  isKeepClosedEnabled: boolean,
  curve: Curve,
  pointer: Vector2,
) {
  if (curve.points.length < 2) {
    OBR.notification.show("Curve is too short.", "ERROR");
    return;
  }

  const points = [...curve.points];
  const firstPoint = points[0];
  const isCurveClosed = isClosed(curve);
  if (isCurveClosed) {
    const lastPoint = points[points.length - 1];
    if (firstPoint.x !== lastPoint.x || firstPoint.y !== lastPoint.y) {
      points.push(firstPoint);
    }
  }

  const c = convertGlobalToItemPosition(curve, pointer);
  let minDistance = Number.MAX_VALUE;
  let cutIndex = 1;
  let cutPosition = c;

  let a = firstPoint;
  for (let index = 1; index < points.length; index++) {
    const b = points[index];
    const d = calculateD(a, b, c);
    const distance = calculateDistance(c, d);
    if (distance < minDistance) {
      minDistance = distance;
      cutIndex = index;
      cutPosition = d;
    }

    a = b;
  }

  if (isCurveClosed) {
    if (!isKeepClosedEnabled) {
      cutClosed(curve, cutIndex, cutPosition);
      return;
    }

    if (cutIndex === 1 || cutIndex === points.length - 1) {
      OBR.notification.show(
        "Cutting closed shapes at the first or last segment can lead to unwanted results.",
        "WARNING",
      );
    }
  }

  const newCurve = cloneCurve(curve);
  newCurve.points = [...points];
  newCurve.points.splice(0, cutIndex);
  newCurve.points.unshift(cutPosition);

  OBR.scene.items.addItems([newCurve]);
  OBR.scene.items.updateItems([curve], (items) => {
    for (let item of items) {
      item.points.splice(cutIndex, item.points.length - cutIndex);
      item.points.push(cutPosition);
    }
  });

  OBR.player.select([newCurve.id]);
}

function cutClosed(curve: Curve, cutIndex: number, cutPosition: Vector2) {
  OBR.scene.items.updateItems([curve], (items) => {
    for (let item of items) {
      item.style = {
        ...item.style,
        closed: false,
        fillOpacity: 0,
      };

      cutIndex %= item.points.length;
      item.points = [
        cutPosition,
        ...item.points.slice(cutIndex).map((a) => ({ ...a })),
        ...item.points.slice(0, cutIndex).map((a) => ({ ...a })),
        cutPosition,
      ];
    }
  });

  OBR.player.select([curve.id]);
}
