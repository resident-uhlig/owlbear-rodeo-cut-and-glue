import OBR, {
  buildShape,
  Curve,
  isCurve,
  isLine,
  Item,
  Line,
  Shape,
  Theme,
  Vector2,
} from "@owlbear-rodeo/sdk";
import { calculateDistance, convertItemToGlobalPosition } from "../cut/math";
import isClosed from "../../obr/items/isClosed";

const INDICATOR_RADIUS = 50;

export type Indicator = {
  shape: Shape;
  remove: () => void;
};

let indicator: Indicator | null = null;

export default function showIndicator(
  theme: Theme,
  pointer: Vector2,
  target?: Item,
) {
  if (!target || (!isLine(target) && !isCurve(target)) || isClosed(target)) {
    removeIndicator();
    return;
  }

  if (!indicator) {
    indicator = createIndicator(theme);
  }

  updateIndicator(indicator.shape, pointer, target);
}

function removeIndicator() {
  if (indicator) {
    indicator.remove();
    indicator = null;
  }
}

export function createIndicator(theme: Theme): Indicator {
  const indicator = buildShape()
    .shapeType("CIRCLE")
    .width(INDICATOR_RADIUS)
    .height(INDICATOR_RADIUS)
    .locked(true)
    .visible(false)
    .build();

  applyTheme(theme, indicator);

  OBR.scene.local.addItems([indicator]);
  const unsubscribeFromTheme = OBR.theme.onChange((theme) =>
    applyTheme(theme, indicator),
  );

  return {
    shape: indicator,
    remove: () => {
      unsubscribeFromTheme();
      OBR.scene.local.deleteItems([indicator.id]);
    },
  };
}

function applyTheme(theme: Theme, indicator: Shape) {
  indicator.style.strokeColor = theme.text.primary;
  indicator.style.fillColor = theme.text.primary;
  indicator.style.fillOpacity = 0.5;
}

export function updateIndicator(
  indicator: Shape,
  pointer: Vector2,
  target: Line | Curve,
) {
  const position = calculatePosition(pointer, target);
  OBR.scene.local.updateItems([indicator], (items) => {
    for (let indicator of items) {
      indicator.position = position;
      indicator.layer = target.layer;
      indicator.zIndex = target.zIndex - 1;
    }
  });
}

function calculatePosition(pointer: Vector2, target: Line | Curve): Vector2 {
  let firstPoint: Vector2;
  let firstDistance: number;
  let lastPoint: Vector2;
  let lastDistance: number;

  if (isLine(target)) {
    firstPoint = convertItemToGlobalPosition(target, target.startPosition);
    firstDistance = calculateDistance(firstPoint, pointer);
    lastPoint = convertItemToGlobalPosition(target, target.endPosition);
    lastDistance = calculateDistance(lastPoint, pointer);
  } else {
    firstPoint = convertItemToGlobalPosition(target, target.points[0]);
    firstDistance = calculateDistance(firstPoint, pointer);
    lastPoint = convertItemToGlobalPosition(
      target,
      target.points[target.points.length - 1],
    );
    lastDistance = calculateDistance(lastPoint, pointer);
  }

  if (firstDistance < lastDistance) {
    return firstPoint;
  }

  return lastPoint;
}
