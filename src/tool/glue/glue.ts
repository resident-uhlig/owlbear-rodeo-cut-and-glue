import OBR, {
  buildCurve,
  Curve,
  isCurve,
  isLine,
  Item,
  Line,
  Theme,
  Vector2,
} from "@owlbear-rodeo/sdk";
import isClosed from "../../obr/items/isClosed";
import { calculateDistance, convertItemToGlobalPosition } from "../cut/math";
import { createIndicator, Indicator, updateIndicator } from "./showIndicator";

type Connection = {
  item: Item;
  points: Vector2[];
};

let indicator: Indicator | null = null;
let connection: Connection | null = null;

export default function glue(theme: Theme, pointer: Vector2, item?: Item) {
  if (!item) {
    reset();
    return true;
  }

  if (!isLine(item) && !isCurve(item)) {
    OBR.notification.show(`Type is not supported: ${item.type}`, "ERROR");
    return;
  }

  if (isClosed(item)) {
    OBR.notification.show("Cannot glue closed nodes.", "ERROR");
    return;
  }

  if (connection === null) {
    const points = getStartPoints(pointer, item);
    connection = {
      points,
      item,
    };

    indicator = createIndicator(theme);
    updateIndicator(indicator.shape, pointer, item);
    return;
  }

  if (connection.item.id === item.id) {
    if (isLine(item)) {
      OBR.notification.show("Cannot close line.", "ERROR");
      return;
    }

    OBR.scene.items.updateItems([item], (items) => {
      for (let item of items) {
        item.style.closed = true;
      }
    });
    reset();
    return;
  }

  if (connection.item.layer !== item.layer) {
    OBR.notification.show("Items are not on same layer.", "WARNING");
  }

  const points = getEndPoints(pointer, item);
  const curve = buildCurve()
    .name(item.name)
    .visible(connection.item.visible && item.visible)
    .locked(connection.item.locked && item.locked)
    .zIndex(Math.max(connection.item.zIndex, item.zIndex))
    .metadata({
      ...connection.item.metadata,
      ...item.metadata,
    })
    .layer(item.layer)
    .points([...connection.points, ...points])
    .build();

  curve.attachedTo = item.attachedTo;
  curve.disableHit = item.disableHit;
  curve.disableAutoZIndex = item.disableAutoZIndex;
  curve.disableAttachmentBehavior = item.disableAttachmentBehavior;
  curve.description = item.description;

  if (isCurve(item)) {
    curve.style = item.style;
  } else {
    curve.style.fillOpacity = 0;
    curve.style.strokeColor = item.style.strokeColor;
    curve.style.strokeOpacity = item.style.strokeOpacity;
    curve.style.strokeWidth = item.style.strokeWidth;
    curve.style.strokeDash = item.style.strokeDash;
    curve.style.tension = 0;
    curve.style.closed = false;
  }

  OBR.scene.items.addItems([curve]);
  OBR.scene.items.deleteItems([connection.item.id, item.id]);
  reset();
}

function reset() {
  if (connection) {
    connection = null;
  }

  if (indicator) {
    indicator.remove();
    indicator = null;
  }
}

function getStartPoints(pointer: Vector2, item: Line | Curve): Vector2[] {
  const points = convertPointsToGlobalPosition(item);

  const firstPoint = points[0];
  const firstDistance = calculateDistance(firstPoint, pointer);

  const lastPoint = points[points.length - 1];
  const lastDistance = calculateDistance(lastPoint, pointer);

  if (firstDistance < lastDistance) {
    return [...points.slice(1).reverse(), firstPoint];
  }

  return [...points.slice(0, -1), lastPoint];
}

function convertPointsToGlobalPosition(item: Line | Curve): Vector2[] {
  if (isLine(item)) {
    const a = convertItemToGlobalPosition(item, item.startPosition);
    const b = convertItemToGlobalPosition(item, item.endPosition);
    return [a, b];
  }

  return item.points.map((point) => convertItemToGlobalPosition(item, point));
}

function getEndPoints(pointer: Vector2, item: Line | Curve): Vector2[] {
  const points = convertPointsToGlobalPosition(item);

  const firstPoint = points[0];
  const firstDistance = calculateDistance(firstPoint, pointer);

  const lastPoint = points[points.length - 1];
  const lastDistance = calculateDistance(lastPoint, pointer);

  if (lastDistance < firstDistance) {
    points.reverse();
  }

  return points;
}
