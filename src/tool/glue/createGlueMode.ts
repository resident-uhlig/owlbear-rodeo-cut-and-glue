import { EXTENSION_ID } from "../../extension/constants";
import OBR, { Theme } from "@owlbear-rodeo/sdk";
import createIconUrl from "../../font-awesome/createIconUrl";
import { DRAWING_TOOL_ID } from "../../obr/tools/constants";
import glue from "./glue";
import showIndicator from "./showIndicator";

export default function createGlueMode(theme: Theme) {
  OBR.theme.onChange((newTheme) => (theme = newTheme));

  const GLUE_MODE_ID = `${EXTENSION_ID}.tool-mode.glue`;
  OBR.tool.createMode({
    id: GLUE_MODE_ID,
    icons: [
      {
        icon: createIconUrl("bandage-solid.svg"),
        label: "Glue",
        filter: {
          activeTools: [DRAWING_TOOL_ID],
          permissions: ["DRAWING_UPDATE"],
        },
      },
    ],
    cursors: [
      {
        cursor: "crosshair",
      },
    ],
    onToolClick: (context, event) => {
      return glue(theme, event.pointerPosition, event.target);
    },
    onToolMove: (context, event) => {
      showIndicator(theme, event.pointerPosition, event.target);
    },
  });
}
