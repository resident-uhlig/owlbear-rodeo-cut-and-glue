import OBR from "@owlbear-rodeo/sdk";
import createCutMode from "./cut/createCutMode";
import createGlueMode from "./glue/createGlueMode";

if (location.hostname === "localhost") {
  (window as any).OBR = OBR;
}

OBR.onReady(() => {
  OBR.theme.getTheme().then((theme) => {
    createCutMode();
    createGlueMode(theme);
  });
});
