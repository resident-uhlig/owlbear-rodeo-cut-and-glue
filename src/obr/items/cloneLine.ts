import { buildLine, Line } from "@owlbear-rodeo/sdk";
import cloneItem from "./cloneItem";

export default function cloneLine(original: Line): Line {
  return cloneItem(buildLine(), original)
    .startPosition({ ...original.startPosition })
    .endPosition({ ...original.endPosition })
    .style(original.style)
    .build();
}
