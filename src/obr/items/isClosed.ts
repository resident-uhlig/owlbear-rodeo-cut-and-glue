import { isCurve, isShape, Item } from "@owlbear-rodeo/sdk";

export default function isClosed(item: Item): boolean {
  return (
    isShape(item) ||
    (isCurve(item) && (item.style.closed || item.style.fillOpacity > 0))
  );
}
