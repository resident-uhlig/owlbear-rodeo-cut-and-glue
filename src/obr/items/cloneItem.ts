import { Item } from "@owlbear-rodeo/sdk";
import { GenericItemBuilder } from "@owlbear-rodeo/sdk/lib/builders/GenericItemBuilder";

export default function cloneItem<T extends GenericItemBuilder<T>>(
  builder: T,
  original: Item,
): T {
  builder
    .name(original.name)
    .visible(original.visible)
    .locked(original.locked)
    .zIndex(original.zIndex)
    .position({ ...original.position })
    .rotation(original.rotation)
    .scale(original.scale)
    .metadata({ ...original.metadata })
    .layer(original.layer);

  if (original.attachedTo) {
    builder.attachedTo(original.attachedTo);
  }

  if (original.disableHit) {
    builder.disableHit(original.disableHit);
  }

  if (original.disableAutoZIndex) {
    builder.disableAutoZIndex(original.disableAutoZIndex);
  }

  if (original.disableAttachmentBehavior) {
    builder.disableAttachmentBehavior([...original.disableAttachmentBehavior]);
  }

  return builder;
}
