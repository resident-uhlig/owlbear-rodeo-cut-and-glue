import { buildCurve, Curve } from "@owlbear-rodeo/sdk";
import cloneItem from "./cloneItem";

export default function cloneCurve(original: Curve): Curve {
  return cloneItem(buildCurve(), original)
    .points([...original.points])
    .style(original.style)
    .build();
}
