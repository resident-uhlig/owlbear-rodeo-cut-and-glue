---
title: $NAME$
description: $DESCRIPTION$
author: $AUTHOR$
image: $GITLAB_PAGES$store/cut-line-or-curve.gif
icon: $GITLAB_PAGES$font-awesome/svgs/scissors-solid.svg
tags:
  - tool
  - drawing
manifest: $GITLAB_PAGES$manifest.json
learn-more: $HOMEPAGE$
---

# $NAME$

## About

This extension adds a tool to cut or glue lines and polygons.

## Features

- Cut lines and polygons apart
- Glue lines and polygons together

## Installation

[Install Your Extension] using the URL [$GITLAB_PAGES$manifest.json](../manifest.json).

[Install Your Extension]: https://docs.owlbear.rodeo/extensions/tutorial-hello-world/install-your-extension/

## Usage

### Permissions

Only players with the [permission] `DRAWING_UPDATE` may cut or glue.

[permission]: https://docs.owlbear.rodeo/extensions/reference/permission

### Scope

User settings are stored per user in the local storage of your browser.

### Supported drawings

- [line](https://docs.owlbear.rodeo/extensions/reference/items/line)
- [curve](https://docs.owlbear.rodeo/extensions/reference/items/curve)
- [shape](https://docs.owlbear.rodeo/extensions/reference/items/shape)
  > ⚠️ Shapes are converted to curves. Circles become square/angular.

> ⚠️ Closed drawings cannot be glued together.

### Cut

1. Select the [drawing tool](https://docs.owlbear.rodeo/docs/drawing/).
2. Select the cut mode (the scissors icon).
3. Click on a line where you want to cut it.

![Demonstration of how to cut a line or a curve]($GITLAB_PAGES$store/cut-line-or-curve.gif)

### Closed / Open Polygons

You can choose to keep polygons closed or cut them open. The indicator and
toggle is located at the top bar and represented by a ring. Check the setting by
hovering the icon. A tooltip shows the current setting.

A ring with a checkmark indicates that closed polygons stay closed. This can
lead to unwanted results. A ring with a notch indicates that closed polygons are
opened.

Click the ring to toggle the setting.

### Glue

1. Select the [drawing tool](https://docs.owlbear.rodeo/docs/drawing/).
2. Select the glue mode (the bandage icon).
3. Click the first node that you want to glue to another. A point will be shown
   to indicate the selected node.
4. Click the second node that you want to glue to the first node.

> ⚠️ Conflicting style and [metadata] might get lost.

[metadata]: https://docs.owlbear.rodeo/extensions/reference/metadata/

![Demonstration of how to glue a line and a curve]($GITLAB_PAGES$store/glue.gif)
